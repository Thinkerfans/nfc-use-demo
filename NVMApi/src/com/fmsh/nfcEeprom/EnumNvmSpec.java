package com.fmsh.nfcEeprom;

public enum EnumNvmSpec {
	
	FM24NC128T1(0),			
	FM24NC128T2(1),
	FM24NC128T3(2),
	FM24NC64T1(3),
	FM24NC64T2(4),
	FM24NC64T3(5),
	FM24NC32T1(6),
	FM24NC32T2(7),
	FM24NC32T3(8);
	
	
	// 枚举类型值
	private int value;
	
	/**
	 * 构造函数
	 * @param v 枚举类型值
	 */
	private EnumNvmSpec(int v){
		value = v;
	}
	
	/**
	 * 获取枚举类型的值
	 * @return 枚举类型值
	 */
	public int getValue(){
		return value;
	}
	
	/**
	 * 根据枚举类型的值获取枚举类型
	 * @param value
	 * @return 枚举类型
	 */
	public static EnumNvmSpec getEnumNvmSpec(int value){
		for(EnumNvmSpec t : EnumNvmSpec.values()) {
			if(t.getValue() == value){
	        	return t;
			}
	    }
		return null;
	} 

}
