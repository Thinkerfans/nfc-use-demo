package com.fmsh.nfcEeprom;

import java.io.IOException;
import java.util.Arrays;

import com.android.common.utils.LogUtil;

import android.annotation.SuppressLint;
import android.nfc.Tag;
import android.nfc.tech.NfcA;
import android.nfc.tech.TagTechnology;
import android.util.Log;

/**
 * 该类封装了在Android平台上访问复旦微产品：FM24NC128Tx的能力<br>
 * 使用习惯类似使用android.nfc.tech包中的技术类型封装类，主要指如下习惯：<br>
 * 1)使用静态方法get()获取本类实例<br>
 * 2)调用封装的功能函数前，首先必须进行connect()连接<br>
 * 3)操作完成后，必须调用close()关闭连接<br>
 * @author shenxiaojing shenxiaojing@fmsh.com.cn
 */
public class NvmDMTech implements TagTechnology {
	
	private static final String TAG = "com.fmsh.nfcEeprom.FM24NCXXTx";
	
	// ISO/IEC 7816-6 IC Manufacturer ID of Fmsh.
	static final byte FM_IC_MANUFACTURER_ID = (byte)0x1D;
	
	static final byte[] ISO_4_CMD_RATS = new byte[]{(byte)0xE0, (byte)0x80};
	
	// DATA MEMORY CMD SET
	static final byte DM_CMD_READ_RF_DATA_RD_LOCK = 0x6A; 
	static final byte DM_CMD_READ_RF_DATA_WR_LOCK = 0x6C;
	static final byte DM_CMD_WRITE_RF_DATA_RD_LOCK = 0x7F;
	static final byte DM_CMD_WRITE_RF_DATA_WR_LOCK = 0x7E;
	static final byte DM_CMD_RF_PWD_AUTH = 0x40;
	static final byte DM_CMD_READ_64B = 0x51;
	static final byte DM_CMD_WRITE_64B = 0x54;

	// FIXED SIZE?
	static final int PAGE_SIZE = 64;
	static final int BLOCK_SIZE = 4;
	static final int PWD_SIZE = 4;
	static final int PACK_SIZE = 2;

	// NAK/ACK
	static final byte NAK = (byte)0xB2;
	static final byte ACK = (byte)0x0A;
	
	NfcA mNfcA;
	
	/**
	 * 静态方法，根据Tag对象返回FM24NC128Tx对象
	 * @param tag
	 * @return FM24NCXXTx对象实例<br>
	 * 注意：并不表示特定容量的某种产品，除非调用setDataMemorySize/setTagMemroySize<br>
	 * 也就是说，不会做相应的边界检查，除非应用告知具体的容量。
	 */
	@SuppressLint("NewApi")
	public static NvmDMTech get(Tag tag) {
		// Check that the given tag is NFC-A compatible
		if (!Arrays.asList(tag.getTechList()).contains("android.nfc.tech.NfcA"))
			return null;

		LogUtil.e(TAG, "Atag txrxsize" + NfcA.get(tag).getMaxTransceiveLength());

		byte[] uid = tag.getId();
		if (uid.length < 2) {
			LogUtil.e(TAG, "Incorrect UID length");
			return null;
		}
		
//		// UID check
//		if ((uid[0] != FM_IC_MANUFACTURER_ID)) {
//			Log.e(TAG, "Not manufacturered by FMSH");
//			return null;
//		}
			
		return new NvmDMTech(NfcA.get(tag));
	}

	
	private NvmDMTech(NfcA nfca) {
		super();
		mNfcA = nfca;
	}

	String toHexString(byte[] data) {
		String hexString = new String();
		for (int index = 0; index < data.length; index++) {
			hexString += String.format("%02x", data[index]);
		}
		
		return hexString;
	}
	
	/*
	 * FWI转换
	 * @param fwi TB(1)高4位
	 * @return FWT
	 */
	int fwiToMilliseconds(int fwi) {
		return 4096 * (0x01 << fwi) / 13560 + 1;
	}
	
	/*
	 * 执行RATS<br>
	 * 设置超时时间为50ms
	 * @throws IOException
	 */
	private void activateLayer4() throws IOException {
		// FSDI = 8 (256 byte), CID = 0
		LogUtil.e(TAG,"Transmitting RATS: " + toHexString(ISO_4_CMD_RATS));
		byte[] ats = mNfcA.transceive(ISO_4_CMD_RATS);
		LogUtil.e(TAG,"Received ATS: " + toHexString(ats));
		
		mNfcA.setTimeout(50);
	}


	/**
	 * 连接FM24NCXXTx对象<br>
	 * 该动作会主动完成Iso14443-4 RATS指令透传
	 */
	public void connect() throws IOException {
		
		LogUtil.e(TAG," mNfcA.isConnected() : " + mNfcA.isConnected());
		mNfcA.connect();
		activateLayer4();
	}
	
	/**
	 * 关闭FM24NCXXTx对象的连接
	 */
	public void close() throws IOException {
		byte[] sDeselect = { (byte) 0xC2 };
		LogUtil.e(TAG,"Transmitting S(DESELECT): " + toHexString(sDeselect));
		byte[] sDeselectResponse = new byte[0];
		try{
			sDeselectResponse = mNfcA.transceive(sDeselect);
			LogUtil.e(TAG,"Received S(DESELECT) response: " + toHexString(sDeselectResponse));
		}catch (IOException e){
		
		}
		
		mNfcA.close();
	}


	/**
	 * 设置超时时间
	 * @param mto 超时时间（单位：ms）
	 * @throws IOException
	 */
	public void setTimeout(int mto) throws IOException {
		mNfcA.setTimeout(mto);
	}

	/**
	 * 返回内部Tag对象
	 */
	public Tag getTag() {
		return mNfcA.getTag();
	}

	/**
	 * 是否已连接Tag对象
	 */
	public boolean isConnected() {
		return mNfcA.isConnected();
	}
	
	/**
	 * 读取非接口读锁定字节<br>
	 * 封装指令：READ_RF_DATA_RD_LOCK (6Ah)
	 * @return RF_DATA_RD_LOCK
	 * @throws IOException
	 */
	public byte[] readRFDataRDLock() throws IOException {

		byte[] cmd = new byte[]{DM_CMD_READ_RF_DATA_RD_LOCK};
		
		LogUtil.e(TAG, "Transmitting P [READ_RF_DATA_RD_LOCK (6Ah)]" + toHexString(cmd));
		byte[] resp = mNfcA.transceive(cmd);	
		LogUtil.e(TAG,"Received:" + toHexString(resp));
		
		if(resp == null || resp.length == 0) {
			throw new IOException("FM24NC128Tx READ_RF_DATA_RD_LOCK failed : NULL response");
		}else if(resp[0] == NAK) {
			throw new IOException("FM24NC128Tx READ_RF_DATA_RD_LOCK failed : NAK response");
		}
		
		return resp;
	}

	/**
	 * 读取非接口写锁定字节<br>
	 * 封装指令：READ_RF_DATA_WR_LOCK (6Ch) 
	 * @return RF_DATA_WR_LOCK
	 * @throws IOException
	 */
	public byte[] readRFDataWRLock() throws IOException {
		
		byte[] cmd = new byte[]{DM_CMD_READ_RF_DATA_WR_LOCK};

		LogUtil.e(TAG, "Transmitting P [READ_RF_DATA_WR_LOCK (6Ch)]" + toHexString(cmd));
		byte[] resp = mNfcA.transceive(cmd);		
		LogUtil.e(TAG,"Received:" + toHexString(resp));
		
		if(resp == null || resp.length == 0) {
			throw new IOException("FM24NC128Tx READ_RF_DATA_WR_LOCK failed : NULL response");
		}else if(resp[0] == NAK) {
			throw new IOException("FM24NC128Tx READ_RF_DATA_WR_LOCK failed : NAK response");
		}
		
		return resp;
	}
	
	
	/**
	 * 写非接口读锁定字节<br>
	 * 封装指令：WRITE_RF_DATA_RD_LOCK (7Fh) 
	 * @param lock RF_DATA_RD_LOCK
	 * @throws IOException
	 */
	public void writeRFDataRDLock(byte[] lock) throws IOException {

		byte[] cmd = new byte[33];
		cmd[0] = DM_CMD_WRITE_RF_DATA_RD_LOCK;
		System.arraycopy(lock, 0, cmd, 1, lock.length);
		
		LogUtil.e(TAG, "Transmitting P [WRITE_RF_DATA_RD_LOCK (7Fh)]" + toHexString(cmd));
		byte[] resp = mNfcA.transceive(new byte[]{DM_CMD_WRITE_RF_DATA_RD_LOCK});	
		LogUtil.e(TAG,"Received:" + toHexString(resp));
		
		if(resp == null || resp.length == 0) {
			throw new IOException("FM24NC128Tx WRITE_RF_DATA_RD_LOCK failed : NULL response");
		}else if(resp[0] == NAK) {
			throw new IOException("FM24NC128Tx WRITE_RF_DATA_RD_LOCK failed : NAK response");
		}else if(resp[0] != ACK) {
			throw new IOException("FM24NC128Tx WRITE_RF_DATA_RD_LOCK failed : Unknown response");
		}
	}
	
	/**
	 * 写非接口写锁定字节<br>
	 * 封装指令：WRITE_RF_DATA_WR_LOCK (7Eh)
	 * @param lock RF_DATA_WR_LOCK
	 * @throws IOException
	 */
	public void writeRFDataWRLock(byte[] lock) throws IOException {
		
		byte[] cmd = new byte[33];
		cmd[0] = DM_CMD_WRITE_RF_DATA_WR_LOCK;
		System.arraycopy(lock, 0, cmd, 1, lock.length);
		
		LogUtil.e(TAG, "Transmitting P [WRITE_RF_DATA_WR_LOCK (7Eh)]" + toHexString(cmd));
		byte[] resp = mNfcA.transceive(cmd);
		LogUtil.e(TAG,"Received:" + toHexString(resp));
		
		if(resp == null || resp.length == 0) {
			throw new IOException("FM24NC128Tx WRITE_RF_DATA_WR_LOCK failed : NULL response");
		}else if(resp[0] == NAK) {
			throw new IOException("FM24NC128Tx WRITE_RF_DATA_WR_LOCK failed : NAK response");
		}else if(resp[0] != ACK) {
			throw new IOException("FM24NC128Tx WRITE_RF_DATA_WR_LOCK failed : Unknown response");
		}
	}
	
	/**
	 * 非接口密钥认证
	 * RFDataWRLock、RFDataRDLock必须经过密钥认证后才可修改
	 * 封装指令：RF_PWD_AUTH (40h) 
	 * @param pwd 4字节密钥
	 * @throws IOException
	 */
	public void rfPWDAuth(byte[] pwd) throws IOException {
		
		if(pwd == null || pwd.length != PWD_SIZE) {
			throw new IOException("invalid pwd.length");
		}
		
		byte[] cmd = new byte[5];
		cmd[0] = DM_CMD_RF_PWD_AUTH;
		System.arraycopy(pwd, 0, cmd, 1, pwd.length);	
		
		LogUtil.e(TAG, "Transmitting P [RF_PWD_AUTH (40h)]" + toHexString(cmd));
		byte[] resp = mNfcA.transceive(cmd);
		LogUtil.e(TAG,"Received:" + toHexString(resp));
		
		if(resp == null || resp.length == 0) {
			throw new IOException("FM24NC128Tx RF_PWD_Auth failed : NULL response");
		}else if(resp[0] == NAK) {
			throw new IOException("FM24NC128Tx RF_PWD_Auth failed : NAK response");
		}else if(resp[0] != ACK) {
			throw new IOException("FM24NC128Tx RF_PWD_Auth failed : Unknown response");
		}
	}
	
	/**
	 * 读取Data Memory一页64字节数据<br>
	 * 封装指令：READ64B (51h) 
	 * @param pageAddr 页地址
	 * @return 64字节数据
	 * @throws IOException
	 */
	public byte[] readPage(int pageAddr) throws IOException {

		byte[] cmd = new byte[]{DM_CMD_READ_64B, (byte)pageAddr};
		LogUtil.e(TAG, "Transmitting P [READ64B (51h)]" + toHexString(cmd));
		byte[] resp = mNfcA.transceive(cmd);
		LogUtil.e(TAG,"Received:" + toHexString(resp));
		
		if(resp == null || resp.length == 0) {
			throw new IOException("FM24NC128Tx EEPROM read failed : NULL response");
		}else if(resp[0] == NAK) {
			throw new IOException("FM24NC128Tx EEPROM read failed : NAK response");
		}else if(resp.length != PAGE_SIZE) {
			throw new IOException("FM24NC128Tx EEPROM read failed : Incorrect length response");
		}
		
		return resp;
	}

	/**
	 * 向Data Memory写一页64字节数据<br>
	 * 封装指令：WRITE64B (54h)
	 * @param pageAddr 页地址
	 * @param data 待写入的64字节数据
	 * @throws IOException
	 */
	public void writePage(int pageAddr, byte[] data) throws IOException {

		if(data == null || data.length != PAGE_SIZE) {
			throw new IOException("invalid data.length");
		}
		
		byte[] cmd = new byte[66];
		cmd[0] = DM_CMD_WRITE_64B;
		cmd[1] = (byte)pageAddr;
		System.arraycopy(data, 0, cmd, 2, PAGE_SIZE);
		LogUtil.e(TAG, "Transmitting P [WRITE64B (54h)]" + toHexString(cmd));
		byte[] resp = mNfcA.transceive(cmd);
		LogUtil.e(TAG,"Received:" + toHexString(resp));

		if(resp == null || resp.length == 0) {
			throw new IOException("FM24NC128Tx EEPROM write failed : NULL response");
		}else if(resp[0] == NAK) {
			throw new IOException("FM24NC128Tx EEPROM write failed : NAK response");
		}else if(resp[0] != ACK) {
			throw new IOException("FM24NC128Tx EEPROM write failed : Unknown response");
		}
	}

}
