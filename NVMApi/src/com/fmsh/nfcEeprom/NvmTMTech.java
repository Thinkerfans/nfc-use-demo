package com.fmsh.nfcEeprom;

import java.io.IOException;
import java.util.Arrays;

import com.android.common.utils.LogUtil;

import android.annotation.SuppressLint;
import android.nfc.Tag;
import android.nfc.tech.NfcA;
import android.nfc.tech.TagTechnology;
import android.util.Log;


/**
 * 该类封装了在Android平台上访问复旦微产品：FM24NC128Tx的能力<br>
 * 使用习惯类似使用android.nfc.tech包中的技术类型封装类，主要指如下习惯：<br>
 * 1)使用静态方法get()获取本类实例<br>
 * 2)调用封装的功能函数前，首先必须进行connect()连接<br>
 * 3)操作完成后，必须调用close()关闭连接<br>
 * @author shenxiaojing shenxiaojing@fmsh.com.cn
 */

public class NvmTMTech implements TagTechnology {

	private static final String TAG = "com.fmsh.nfcEeprom.FM24NCXXTx";

	// ISO/IEC 7816-6 IC Manufacturer ID of Fmsh.
	static final byte FM_IC_MANUFACTURER_ID = (byte) 0x1D;

	static final byte[] ISO_4_CMD_RATS = new byte[] { (byte) 0xE0, (byte) 0x80 };

	// DATA MEMORY CMD SET
	static final byte TM_CMD_COMPATIBILITY_WRITE = (byte) 0xA0;
	static final byte TM_CMD_FAST_READ = 0x3A;
	static final byte TM_CMD_PWD_AUTH = 0x1B;

	// FIXED SIZE?
	static final int PAGE_SIZE = 64;
	static final int BLOCK_SIZE = 4;
	static final int PWD_SIZE = 4;
	static final int PACK_SIZE = 2;

	// NAK/ACK
	static final byte NAK = (byte) 0xB2;
	static final byte ACK = (byte) 0x0A;

	NfcA mNfcA;

	
	/**
	 * 静态方法，根据Tag对象返回FM24NC128Tx对象
	 * @param tag
	 * @return FM24NCXXTx对象实例<br>
	 * 注意：并不表示特定容量的某种产品，除非调用setDataMemorySize/setTagMemroySize<br>
	 * 也就是说，不会做相应的边界检查，除非应用告知具体的容量。
	 */
	@SuppressLint("NewApi")
	public static NvmTMTech get(Tag tag) {
		// Check that the given tag is NFC-A compatible
		if (!Arrays.asList(tag.getTechList()).contains("android.nfc.tech.NfcA"))
			return null;

		LogUtil.d(TAG, "Atag txrxsize" + NfcA.get(tag).getMaxTransceiveLength());

		byte[] uid = tag.getId();
		if (uid.length < 2) {
			Log.e(TAG, "Incorrect UID length");
			return null;
		}

		// // UID check
		// if ((uid[0] != FM_IC_MANUFACTURER_ID)) {
		// Log.e(TAG, "Not manufacturered by FMSH");
		// return null;
		// }

		return new NvmTMTech(NfcA.get(tag));
	}

	/*
	 * ˽�й��췽��
	 * @param nfca
	 */
	private NvmTMTech(NfcA nfca) {
		super();
		mNfcA = nfca;
	}

	String toHexString(byte[] data) {
		String hexString = new String();
		for (int index = 0; index < data.length; index++) {
			hexString += String.format("%02x", data[index]);
		}

		return hexString;
	}

	/*
	 * FWI转换
	 * @param fwi TB(1)高4位
	 * @return FWT
	 */
	int fwiToMilliseconds(int fwi) {
		return 4096 * (0x01 << fwi) / 13560 + 1;
	}


	/**
	 * 连接FM24NCXXTx对象<br>
	 * 该动作会主动完成Iso14443-4 RATS指令透传
	 */
	public void connect() throws IOException {
		mNfcA.connect();
	}

	
	/**
	 * 关闭FM24NCXXTx对象的连接
	 */
	public void close() throws IOException {
		mNfcA.close();
	}


	/**
	 * 设置超时时间
	 * @param mto 超时时间（单位：ms）
	 * @throws IOException
	 */
	public void setTimeout(int mto) throws IOException {
		mNfcA.setTimeout(mto);
	}

	/**
	 * 返回内部Tag对象
	 */
	public Tag getTag() {
		return mNfcA.getTag();
	}

	/**
	 * 是否已连接Tag对象
	 */
	public boolean isConnected() {
		return mNfcA.isConnected();
	}
	

	/**
	 * 非接口密钥认证
	 * RFDataWRLock、RFDataRDLock必须经过密钥认证后才可修改
	 * 封装指令：RF_PWD_AUTH (40h) 
	 * @param pwd 4字节密钥
	 * @throws IOException
	 */
	public void compalibilityWrite(int blockAddr, byte[] data)
			throws IOException {

		if (data == null || data.length != BLOCK_SIZE) {
			throw new IOException("invalid data.length");
		}

		byte[] cmd = new byte[2];
		cmd[0] = TM_CMD_COMPATIBILITY_WRITE;
		cmd[1] = (byte) blockAddr;
		LogUtil.d(TAG, "Transmitting P [COMPALIBILITY WRITE(A0h)] - Part1" + toHexString(cmd));
		byte[] resp = mNfcA.transceive(cmd);
		LogUtil.d(TAG, "Received:" + toHexString(resp));

		if (resp == null || resp.length == 0) {
			throw new IOException(
					"CompalibilityWrite failed - Part1: NULL response");
		} else if (resp[0] == NAK) {
			throw new IOException(
					"CompalibilityWrite failed - Part1: NAK response");
		} else if (resp[0] != ACK) {
			throw new IOException(
					"CompalibilityWrite failed - Part1: Unknown response");
		}

		cmd = new byte[16];
		Arrays.fill(cmd, (byte) 0x00);
		System.arraycopy(data, 0, cmd, 0, BLOCK_SIZE);
		LogUtil.d(TAG, "Transmitting P [COMPALIBILITY WRITE(A0h)] - Part2" + toHexString(cmd));
		resp = mNfcA.transceive(cmd);
		LogUtil.d(TAG, "Received:" + toHexString(resp));

		if (resp == null || resp.length == 0) {
			throw new IOException(
					"CompalibilityWrite failed - Part2: NULL response");
		} else if (resp[0] == NAK) {
			throw new IOException(
					"CompalibilityWrite failed - Part2: NAK response");
		} else if (resp[0] != ACK) {
			throw new IOException(
					"CompalibilityWrite failed - Part2: Unknown response");
		}

	}

	
	/**
	 * 读取Data Memory一页64字节数据<br>
	 * 封装指令：READ64B (51h) 
	 * @param pageAddr 页地址
	 * @return 64字节数据
	 * @throws IOException
	 */
	public byte[] fastRead(int startBlock, int endBlock) throws IOException {

		if (endBlock <= startBlock) {
			throw new IOException("invalid param");
		}

		byte[] cmd = new byte[] { TM_CMD_FAST_READ, (byte) startBlock,
				(byte) endBlock };
		LogUtil.d(TAG, "Transmitting P [FAST READ (3Ah)]" + toHexString(cmd));
		byte[] resp = mNfcA.transceive(cmd);
		LogUtil.d(TAG, "Received:" + toHexString(resp));

		if (resp == null || resp.length == 0) {
			throw new IOException(
					"Tag Memory read failed : NULL response");
		} else if (resp[0] == NAK && resp.length == 1) {
			throw new IOException(
					"Tag Memory read failed : NAK response");
		}
		/*
		 * else if(resp.length != PAGE_SIZE) { throw new
		 * IOException("FM24NC128Tx EEPROM read failed : Incorrect length response"
		 * ); }
		 */
		return resp;

	}

	/**
	 * 向Data Memory写一页64字节数据<br>
	 * 封装指令：WRITE64B (54h)
	 * @param pageAddr 页地址
	 * @param data 待写入的64字节数据
	 * @throws IOException
	 */
	public byte[] pwdAuth(byte[] pwd) throws IOException {

		if (pwd == null || pwd.length != PWD_SIZE) {
			throw new IOException(
					"FM24NC128Tx PWD_Auth failed : Invalid pwd length");
		}

		byte[] cmd = new byte[5];
		cmd[0] = TM_CMD_PWD_AUTH;
		System.arraycopy(pwd, 0, cmd, 1, pwd.length);

		LogUtil.d(TAG, "Transmitting P [PWD_AUTH (1Bh)]" + toHexString(cmd));
		byte[] resp = mNfcA.transceive(cmd);
		LogUtil.d(TAG, "Received:" + toHexString(resp));

		if (resp == null || resp.length == 0) {
			throw new IOException("FM24NC128Tx PWD_Auth failed : NULL response");
		} else if (resp[0] == NAK) {
			throw new IOException("FM24NC128Tx PWD_Auth failed : NAK response");
		} else if (resp.length != PACK_SIZE) {
			throw new IOException(
					"FM24NC128Tx PWD_Auth failed : Incorrect length response");
		}

		return resp;
	}
}
