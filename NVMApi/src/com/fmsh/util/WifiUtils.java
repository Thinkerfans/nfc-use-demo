
package com.fmsh.util;
import java.util.Arrays;

import android.util.Log;

import com.android.common.utils.LogUtil;
import com.google.common.primitives.Bytes;

public class WifiUtils {
	private static final String TAG  = "WifiUtils ";
	private static final String DEFAULT_CHARSET_NAME = "UTF-8";
	
	public static enum ENCRYPTION_TYPE {
		ENCRYPTION_TYPE_WPA("WPA","100F00020008"),
		ENCRYPTION_TYPE_WEP("WEP","100F00020002"),
		ENCRYPTION_TYPE_NONE("NONE","100F00020001");
		
		private String type;
		private String typeTlv;
		private ENCRYPTION_TYPE(String type,String typeTlv) {
			this.type = type;
			this.typeTlv = typeTlv;
		}
		
		public static ENCRYPTION_TYPE getType(String typeTlv) {
			for (ENCRYPTION_TYPE item : ENCRYPTION_TYPE.values()) {
				if (item.typeTlv.equals(typeTlv)) {
					return item;
				}
			}
			return null;
		}
	}
	
	public static final String VERSION_T = "104A";
	public static final String VERSION_TLV = "104A000110";
	public static final String SSID_T = "1045";
	public static final String NETWORK_KEY_T = "1027";
	public static final String CREDENTIAL_T = "100E";
	public static final String ENCRYPTION_TYPE_T = "100F";
	
	public static byte[] wifiToNdef(String ssid,String password,ENCRYPTION_TYPE encryptionType) {		
		LogUtil.e(TAG, "enter method wifiToNdef ");
		if (ssid == null || ssid.length() == 0 ) {
			return null;
		}
		
		try {
			byte[] ssidByte = ssid.getBytes(DEFAULT_CHARSET_NAME);
			byte[] ssidLen = FM_Bytes.intToBytes(ssidByte.length, 2);
			byte[] ssidTlv = Bytes.concat(FM_Bytes.hexStringToBytes(SSID_T),
					ssidLen, ssidByte);
			byte[] passwordTlv = null;
			if (password != null && password.length() > 0) {
				byte[] passwordByte = password.getBytes(DEFAULT_CHARSET_NAME);
				byte[] passwordLen = FM_Bytes.intToBytes(passwordByte.length, 2);
				passwordTlv = Bytes.concat(FM_Bytes.hexStringToBytes(NETWORK_KEY_T),
						passwordLen, passwordByte);
			}
			
			
			byte[] encryptionTypeTlv = FM_Bytes.hexStringToBytes(encryptionType.typeTlv);
			byte[] credentialValue = null;
			if (passwordTlv != null) {
				credentialValue = Bytes.concat(ssidTlv,passwordTlv,encryptionTypeTlv);
			} else {
				credentialValue = Bytes.concat(ssidTlv,encryptionTypeTlv);
			}
			byte[] credentialLen = FM_Bytes.intToBytes(credentialValue.length, 2);
			byte[] credentialType = FM_Bytes.hexStringToBytes(CREDENTIAL_T);
			byte[] credentialTlv = Bytes.concat(credentialType, credentialLen,
					credentialValue);
			
			byte[] versionTlv = FM_Bytes.hexStringToBytes(VERSION_TLV);
			
			byte[] payload = Bytes.concat(versionTlv, credentialTlv);
			LogUtil.e(TAG, "method wifiToNdef progress OK  "+payload);
			return payload;
		} catch (Exception e) {
			LogUtil.e(TAG, "wifiToNdef 设置异常"+e.toString());
			return null;
		}
		
	}
	
	public static String[] ndefToWifi(byte[] ndef) {
		
		if (ndef == null || ndef.length == 0) {
			return null;
		}
				
		byte[] versionTlv = FM_Bytes.hexStringToBytes(VERSION_TLV);
		byte[] credentialT = FM_Bytes.hexStringToBytes(CREDENTIAL_T);
		
		byte[] ssidT = FM_Bytes.hexStringToBytes(SSID_T);
		byte[] encryptTypeT = FM_Bytes
				.hexStringToBytes(ENCRYPTION_TYPE_T);
		byte[] networkKeyT = FM_Bytes.hexStringToBytes(NETWORK_KEY_T);
		
		if (!Arrays.equals(versionTlv, FM_Bytes.copyOf(ndef, versionTlv.length))) {
			return null;
		}
		
		byte[] credentialTlv = FM_Bytes.copyOfRange(ndef, versionTlv.length, ndef.length);
		if (!Arrays.equals(credentialT, FM_Bytes.copyOf(credentialTlv, credentialT.length))) {
			return null;
		}
		
		byte[] remainData = FM_Bytes.copyOfRange(credentialTlv, credentialT.length + 2, credentialTlv.length);
		if (!Arrays.equals(ssidT, FM_Bytes.copyOf(remainData, ssidT.length))) {
			return null;
		}
		byte[] ssidL = FM_Bytes.copyOfRange(remainData, ssidT.length, ssidT.length + 2);
		byte[] ssidV = FM_Bytes.copyOfRange(remainData, ssidT.length + 2, ssidT.length + 2 + FM_Bytes.bytesToInt(ssidL));
		
		byte[] passwordV = null;
		remainData = FM_Bytes.copyOfRange(remainData, ssidT.length + 2 + FM_Bytes.bytesToInt(ssidL), remainData.length);
		if (Arrays.equals(networkKeyT, FM_Bytes.copyOf(remainData, networkKeyT.length))) {
			byte[] passwordL = FM_Bytes.copyOfRange(remainData, networkKeyT.length, networkKeyT.length + 2);
			passwordV = FM_Bytes.copyOfRange(remainData, networkKeyT.length + 2, networkKeyT.length + 2 + FM_Bytes.bytesToInt(passwordL));
			remainData = FM_Bytes.copyOfRange(remainData, networkKeyT.length + 2 + FM_Bytes.bytesToInt(passwordL), remainData.length);
		}
		
		if (!Arrays.equals(encryptTypeT, FM_Bytes.copyOf(remainData, encryptTypeT.length))) {
			return null;
		}		
		//byte[] encryptTypeL = FM_Bytes.copyOfRange(remainData, encryptTypeT.length, encryptTypeT.length + 2);
		//byte[] encryptTypeV = FM_Bytes.copyOfRange(remainData, encryptTypeT.length + 2, encryptTypeT.length + 2 + FM_Bytes.bytesToInt(encryptTypeL));
		
		return new String[]{new String(ssidV),passwordV == null?null:new String(passwordV),FM_Bytes.bytesToHexString(remainData)};
	}
}
