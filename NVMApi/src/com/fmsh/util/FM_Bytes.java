package com.fmsh.util;

import com.android.common.utils.LogUtil;

/**
 * 用于JNI数据字节数组传递及静态Byte工具方法
 * 
 * @author james gao
 * 
 */
public class FM_Bytes {

	// 定义并初始化日志处理器

	private static String hexString = "0123456789ABCDEF";

	private static String clazzName = FM_Bytes.class.getName();

	/**
	 * 字节数组
	 */
	public byte[] data;

	public FM_Bytes() {
		// 初始化一个空字节数组
		data = new byte[0];
	}

	public FM_Bytes(String hexStr) {
		// 初始化一个空字节数组
		data = new byte[0];
		// 由构造函数生成传入的十六进制字符串初始化一个字节数组
		valueof(hexStr);
	}

	/**
	 * 获取字节数组长度
	 * 
	 * @return 字节数字长度
	 */
	public int length() {
		return data.length;
	}

	/**
	 * 清除数据，释放分配空间
	 */
	public void clear() {

		if (data != null) {
			if (data.length > 0) {
				// 一个0字节数组
				data = new byte[0];
			}
		} else {
			// 一个0字节数组
			data = new byte[0];
		}

	}

	/**
	 * 预分配空间
	 * 
	 * @param len
	 *            预分配空间大小
	 * @param c
	 *            预装填数据
	 * @return 分配空间大小
	 */
	public int preplace(int len, byte c) {
		if (data.length != len) {
			data = new byte[len];
		}
		for (int i = 0; i < len; i++) {
			data[i] = c;
		}

		return data.length;
	}

	/**
	 * 预分配空间
	 * 
	 * @param len
	 *            预分配空间大小
	 * @return 分配空间大小
	 */
	public int preplace(int len) {
		return preplace(len, (byte) 0);
	}

	/**
	 * 从指定位置设置数据
	 * 
	 * @param fromIndex
	 * @param srcData
	 * @return 字节数组
	 */
	public boolean setData(int fromIndex, byte[] srcData) throws Exception {

		// 边界检查
		int endIndex = fromIndex + srcData.length;
		if (endIndex > data.length) {
			// 越界
			throw new Exception("overstep the boundary");
		}

		// 数据复制
		for (int i = 0; i < srcData.length; i++) {
			data[fromIndex + i] = srcData[i];
		}

		return true;
	}

	/**
	 * 从指定位置设置数据
	 * 
	 * @param fromIndex
	 *            指定起始位置
	 * @param srcData
	 *            源数据字节数组
	 * @param start
	 *            字节数组起始下标
	 * @param datalen
	 *            复制数据长度
	 * @return 字节数组
	 */
	public boolean setData(int fromIndex, byte[] srcData, int start, int datalen)
			throws Exception {

		// 参数有效性检测
		if (start < 0 || start + datalen > srcData.length) {
			// 复制数据参数无效
			throw new Exception("invalid parameters");

		}

		// 边界检查
		int endIndex = fromIndex + datalen;
		if (endIndex > data.length) {
			// 越界
			throw new Exception("overstep the boundary");
		}

		// 数据复制
		for (int i = 0; i < datalen; i++) {
			data[fromIndex + i] = srcData[start + i];
		}

		return true;
	}

	/**
	 * 从指定位置设置数据
	 * 
	 * @param fromIndex
	 * @param hexStr
	 * @return 设置结果
	 */
	public boolean setData(int fromIndex, String hexStr) throws Exception {

		int maxLen = (hexStr.length() + 1) / 2;
		if (maxLen > 0) {
			byte[] srcData = new byte[maxLen];
			String hexStr2 = hexStr.toUpperCase();

			int hexCount = 0;
			int byteLen = 0;
			int pos = -1;
			byte b = 0;
			for (int i = 0; i < hexStr2.length(); i++) {
				char c = hexStr2.charAt(i);
				pos = hexString.indexOf(c);
				if (pos != -1) {
					// 有效十六进制数基数
					hexCount++;

					if (hexCount % 2 == 1) {
						// 高位数据
						b |= pos << 4;
					} else {
						// 低位数据
						b |= pos;

						// 数组数据装填
						srcData[byteLen++] = b;
						// 数据复位
						b = 0;
					}
				}
			}

			if (hexCount % 2 == 1) {
				// 若HEX字符串非2位对其，在临时数组空间中装填最后一个字节数据
				srcData[byteLen++] = b;
			}

			// 边界检查
			int endIndex = fromIndex + byteLen;
			if (endIndex > data.length) {
				// 越界
				throw new Exception("overstep the boundary");
			}

			// 数据复制
			for (int i = 0; i < byteLen; i++) {
				data[fromIndex + i] = srcData[i];
			}
		}

		return true;

	}

	public void copy(int length) throws Exception {
		if (length < 0 || length < data.length) {
			// 越界
			throw new Exception("overstep the boundary");
		}
		data = FM_Bytes.copyOf(data, length);
	}

	/**
	 * 十六进制数字符串转化为byte数组
	 * 
	 * @param hexStr
	 * @return 转换结果
	 */
	public boolean valueof(String hexStr) {
		int maxLen = (hexStr.length() + 1) / 2;
		if (maxLen > 0) {
			byte[] dataTmp = new byte[maxLen];
			String hexStr2 = hexStr.toUpperCase();

			int hexCount = 0;
			int byteLen = 0;
			int pos = -1;
			byte b = 0;
			for (int i = 0; i < hexStr2.length(); i++) {
				char c = hexStr2.charAt(i);
				pos = hexString.indexOf(c);
				if (pos != -1) {
					// 有效十六进制数基数
					hexCount++;

					if (hexCount % 2 == 1) {
						// 高位数据
						b |= pos << 4;
					} else {
						// 低位数据
						b |= pos;

						// 数组数据装填
						dataTmp[byteLen++] = b;
						// 数据复位
						b = 0;
					}
				}
			}

			if (hexCount % 2 == 1) {
				// 若HEX字符串非2位对其，在临时数组空间中装填最后一个字节数据
				dataTmp[byteLen++] = b;
			}

			// 数据从临时空间复制到存储空间
			if (byteLen == maxLen) {
				// 空间复制
				data = dataTmp;
			} else if (byteLen == 0) {
				// 重新分配数据空间（数组长度为0）
				data = new byte[0];
			} else {
				// 申请空间，数据复制
				data = new byte[byteLen];
				for (int i = 0; i < byteLen; i++) {
					data[i] = dataTmp[i];
				}
			}

		} else {
			// 重新分配数据空间（数组长度为0）
			data = new byte[0];
		}

		return true;
	}

	/**
	 * 数组数据16进制字符串输出
	 * 
	 * @param separator
	 *            字节分隔符，'\0'表示无分隔符
	 * @return Hex串
	 */
	public String toHexString(char separator) {
		StringBuilder hexStr = new StringBuilder();
		for (int i = 0; i < data.length; i++) {
			// 将字节数组中每个字节拆解成2位16进制整数
			byte b = data[i];
			hexStr.append(hexString.charAt((b & 0xf0) >> 4));
			hexStr.append(hexString.charAt((b & 0x0f)));
			if (separator != '\0') {
				hexStr.append(separator);
			}
		}

		return hexStr.toString();
	}

	public String toString() {
		char separator = ' ';
		return toHexString(separator);
	}

	/**
	 * 小段模式计算字节数组0下标开始2字节的短整形数<br/>
	 * 字节数组长度不足2字节都将抛出异常
	 * 
	 * @return 短整形数值
	 */
	public short shortValue() throws Exception {
		// 第一个字节，小段模式
		return shortValue(0, false);
	}

	/**
	 * 指定模式计算字节数组0下标开始2字节的短整形数<br/>
	 * 字节数组长度不足2字节都将抛出异常
	 * 
	 * @param bigEdian
	 *            大端模式为true，小端模式为false
	 * @return 短整形数值
	 */
	public short shortValue(boolean bigEdian) throws Exception {
		// 第一个字节
		return shortValue(0, bigEdian);
	}

	/**
	 * 指定模式计算指定数组起始下标开始2字节的短整形数<br/>
	 * 起始数组下标越界或字节数组长度不足都将抛出异常
	 * 
	 * @param fromIndex
	 *            数据起始位置（偏移下标，0开始）
	 * @param bigEdian
	 *            大端模式为true，小端模式为false
	 * @return 短整形数值
	 */
	public short shortValue(int fromIndex, boolean bigEdian) throws Exception {

		int endIndex = fromIndex + 2;
		if (endIndex > data.length) {
			// 越界
			throw new Exception("overstep the boundary");
		}

		short n = 0;
		if (bigEdian) {
			// 大端模式
			n = data[fromIndex];
			n <<= 8;
			n |= data[fromIndex + 1] & (0xFF);

			// for (int i = 1; i < 2; i++) {
			// n <<= 8;
			// n |= data[fromIndex+i] & (0xFF);
			// }
		} else {
			// 小端模式
			n = data[fromIndex + 1];
			n <<= 8;
			n |= data[fromIndex] & (0xFF);

			// for (int i = 2; i > 1; i--) {
			// n <<= 8;
			// n |= data[fromIndex+i] & (0xFF);
			// }
		}

		// 返回数值
		return n;
	}

	/**
	 * 小段模式计算字节数组0下标开始4字节的整形数<br/>
	 * 字节数组长度不足4字节都将抛出异常
	 * 
	 * @return 整形数值
	 */
	public int intValue() throws Exception {
		// 从第1个字节开始4字节，小段模式
		return intValue(0, false);
	}

	/**
	 * 指定模式计算字节数组0下标开始4字节的整形数<br/>
	 * 字节数组长度不足4字节都将抛出异常
	 * 
	 * @param bigEdian
	 *            大端模式为true，小端模式为false
	 * @return 整形数值
	 */
	public int intValue(boolean bigEdian) throws Exception {
		// 第一个字节
		return intValue(0, bigEdian);
	}

	/**
	 * 指定模式计算指定数组起始下标开始4字节的整形数<br/>
	 * 起始数组下标越界或字节数组长度不足都将抛出异常
	 * 
	 * @param fromIndex
	 *            数据起始位置（偏移下标，0开始）
	 * @param bigEdian
	 *            大端模式为true，小端模式为false
	 * @return 整形数值
	 */
	public int intValue(int fromIndex, boolean bigEdian) throws Exception {
		int endIndex = fromIndex + 4;
		if (endIndex > data.length) {
			// 越界
			throw new Exception("overstep the boundary");
		}

		int n = 0;
		if (bigEdian) {
			// 大端模式
			n = data[fromIndex];
			for (int i = 1; i < 4; i++) {
				n <<= 8;
				n |= data[fromIndex + i] & (0xFF);
			}
		} else {
			// 小端模式
			n = data[fromIndex + 3];
			for (int i = 2; i >= 0; i--) {
				n <<= 8;
				n |= data[fromIndex + i] & (0xFF);
			}
		}

		// 返回数值
		return n;
	}

	/**
	 * 小段模式计算字节数组0下标开始8字节的长形数<br/>
	 * 字节数组长度不足8字节都将抛出异常
	 * 
	 * @return 长整形数值
	 */
	public long longValue() throws Exception {
		// 第一个字节，小端模式
		return longValue(0, false);
	}

	/**
	 * 指定模式计算字节数组0下标开始8字节的长整形数<br/>
	 * 字节数组长度不足8字节都将抛出异常
	 * 
	 * @param bigEdian
	 *            大端模式为true，小端模式为false
	 * @return 长整形数值
	 */
	public long longValue(boolean bigEdian) throws Exception {
		// 第一个字节
		return longValue(0, bigEdian);
	}

	/**
	 * 指定模式计算指定数组起始下标开始8字节的长整形数<br/>
	 * 起始数组下标越界或字节数组长度不足都将抛出异常
	 * 
	 * @param fromIndex
	 *            数据起始位置（偏移下标，0开始）
	 * @param bigEdian
	 *            大端模式为true，小端模式为false
	 * @return 长整形数值
	 */
	public long longValue(int fromIndex, boolean bigEdian) throws Exception {
		int endIndex = fromIndex + 8;
		if (endIndex > data.length) {
			// 越界
			throw new Exception("overstep the boundary");
		}

		long n = 0;
		if (bigEdian) {
			// 大端模式
			n = data[fromIndex];
			for (int i = 1; i < 8; i++) {
				n <<= 8;
				n |= data[fromIndex + i] & (0xFF);
			}
		} else {
			// 小端模式
			n = data[fromIndex + 7];
			for (int i = 6; i >= 0; i--) {
				n <<= 8;
				n |= data[fromIndex + i] & (0xFF);
			}
		}

		// 返回数值
		return n;
	}

	// =================================================================================
	// 以下为静态方法区
	// =================================================================================

	/**
	 * 字节数组异或
	 * 
	 * @param data1
	 *            数组1
	 * @param data2
	 *            数组2
	 * @return 异或结果
	 */
	public static byte[] xor(byte[] data1, byte[] data2) {

		if (data1 == null || data2 == null) {
			LogUtil.e(clazzName, "异或时，数组为null");
			return null;
		}

		if (data1.length != data2.length) {
			LogUtil.e(clazzName, "异或时，byte数组长度不等");
			return null;
		}

		byte[] result = data1.clone();

		for (int i = 0; i < result.length; i++) {
			result[i] ^= data2[i];
		}
		return result;
	}

	/**
	 * 字节数组取反
	 * 
	 * @param data
	 *            字节数组
	 * @return 取反结果字节数组
	 */
	public static byte[] not(byte[] data) {

		if (data == null) {
			LogUtil.e(clazzName, "取反时，数组为null");
			return null;
		}

		byte[] result = data.clone();

		for (int i = 0; i < result.length; i++) {
			result[i] = (byte) ~(data[i]);
		}

		return result;
	}

	/**
	 * 字节数组与运算
	 * 
	 * @param data1
	 *            数组1
	 * @param data2
	 *            数组2
	 * @return 与运算结果
	 */
	public static byte[] and(byte[] data1, byte[] data2) {

		if (data1 == null || data2 == null) {
			LogUtil.e(clazzName, "与运算时，数组为null");
			return null;
		}

		if (data1.length != data2.length) {
			LogUtil.e(clazzName, "与运算时，byte数组长度不等");
			return null;
		}

		byte[] result = data1.clone();

		for (int i = 0; i < result.length; i++) {
			result[i] &= data2[i];
		}
		return result;
	}

	/**
	 * 或运算
	 * 
	 * @param data1
	 *            数组1
	 * @param data2
	 *            数组2
	 * @return 或运算结果
	 */
	public static byte[] or(byte[] data1, byte[] data2) {

		if (data1 == null || data2 == null) {
			LogUtil.e(clazzName, "或运算时，数组为null");
			return null;
		}

		if (data1.length != data2.length) {
			LogUtil.e(clazzName, "或运算时，byte数组长度不等");
			return null;
		}

		byte[] result = data1.clone();

		for (int i = 0; i < result.length; i++) {
			result[i] |= data2[i];
		}

		return result;
	}

	/**
	 * 字节数组合并
	 * 
	 * @param data1
	 *            byte数组
	 * @param data2
	 *            byte数组
	 * @return 合并后的数组
	 */
	public static byte[] join(byte[] data1, byte[] data2) {
		if (data1 == null || data2 == null) {
			LogUtil.e(clazzName, "字节数组合并时，数组为null");
			return data1;
		}

		int sourceLength = data1.length;
		int length = data1.length + data2.length;
		byte[] bytes = new byte[length];

		for (int i = 0; i < sourceLength; i++) {
			bytes[i] = data1[i];
		}

		for (int i = 0; i < data2.length; i++) {
			bytes[sourceLength + i] = data2[i];
		}

		return bytes;
	}

	/**
	 * 十六进制数字符串转化为byte数组 例：hexString："321"，转为字节数组为0x32, 0x10
	 * 
	 * @param hexStr
	 *            hex字符串
	 * @return byte数组
	 */
	public static byte[] hexStringToBytes(String hexStr) {
		if (hexStr == null || hexStr.length() < 1) {
			return null;
		}
		byte[] data;

		int maxLen = (hexStr.length() + 1) / 2;
		if (maxLen > 0) {
			byte[] dataTmp = new byte[maxLen];
			String hexStr2 = hexStr.toUpperCase();

			int hexCount = 0;
			int byteLen = 0;
			int pos = -1;
			byte b = 0;
			for (int i = 0; i < hexStr2.length(); i++) {
				char c = hexStr2.charAt(i);
				pos = hexString.indexOf(c);
				if (pos != -1) {
					// 有效十六进制数基数
					hexCount++;

					if (hexCount % 2 == 1) {
						// 高位数据
						b |= pos << 4;
					} else {
						// 低位数据
						b |= pos;

						// 数组数据装填
						dataTmp[byteLen++] = b;
						// 数据复位
						b = 0;
					}
				}
			}

			if (hexCount % 2 == 1) {
				// 若HEX字符串非2位对其，在临时数组空间中装填最后一个字节数据
				dataTmp[byteLen++] = b;
			}

			// 数据从临时空间复制到存储空间
			if (byteLen == maxLen) {
				// 空间复制
				data = dataTmp;
			} else if (byteLen == 0) {
				// 重新分配数据空间（数组长度为0）
				data = new byte[0];
			} else {
				// 申请空间，数据复制
				data = new byte[byteLen];
				for (int i = 0; i < byteLen; i++) {
					data[i] = dataTmp[i];
				}
			}

		} else {
			// 重新分配数据空间（数组长度为0）
			data = new byte[0];
		}

		return data;
	}

	/**
	 * 字节数组转换为HEX字符串，可指定前缀以及分隔符，前缀一般使用0x
	 * 
	 * @param array
	 *            字节数组
	 * @param prefix
	 *            前缀，若无传入null
	 * @param split
	 *            分隔符，若无传入null
	 * @return Hex字符串
	 */
	public static String bytesToHexString(byte[] array, String prefix,
			String split) {
		StringBuilder ret = new StringBuilder();
		for (int i = 0; i < array.length; i++) {
			String hex = Integer.toHexString(array[i] & 0xFF);
			if (hex.length() == 1) {
				hex = '0' + hex;
			}

			if (prefix != null && !"".equals(prefix)) {
				ret.append(prefix);
			}

			ret.append(hex.toUpperCase());

			if (split != null && !"".equals(split)) {
				ret.append(split);
			}
		}

		return ret.toString();
	}

	/**
	 * 字节数组转换为HEX字符串,默认无前缀，无分隔符
	 * 
	 * @param array
	 *            字节数组
	 * @return Hex字符串
	 */
	public static String bytesToHexString(byte[] array) {
		if (array == null) {
			return null;
		}
		return bytesToHexString(array, "", "");
	}

	/**
	 * 转换字节数组为整数，默认大端模式
	 * 
	 * @param bytes
	 *            需转换的byte数组
	 * @return 整数 ，返回-1表示转换失败
	 */
	public static int bytesToInt(byte[] bytes) {
		return bytesToInt(bytes, true);
	}

	/**
	 * 转换字节数组为整数
	 * 
	 * @param bytes
	 *            需要转换的字节数组格式
	 * @param bigEndian
	 *            大端模式为true，小端模式为false
	 * @return 整形数值，返回-1表示转换失败
	 */
	public static int bytesToInt(byte[] bytes, boolean bigEndian) {
		if (bytes == null) {
			LogUtil.e(clazzName, "字节数组转成十进制整数时，数组为null");
			return -1;
		}
		if (bytes.length < 1) {
			LogUtil.e(clazzName, "字节数组转成十进制整数时，数组长度为0");
			return -1;
		}
		// 若只有一个字节
		if (bytes.length == 1) {
			return (int) (bytes[0] & 0xFF);
		}

		int n = 0;
		if (bigEndian) {
			// 大端模式
			n = bytes[0];
			for (int i = 1; i < bytes.length; i++) {
				n <<= 8;
				n |= bytes[i] & (0xFF);
			}
		} else {
			// 小端模式
			n = bytes[bytes.length - 1];
			for (int i = bytes.length - 2; i >= 0; i--) {
				n <<= 8;
				n |= bytes[i] & (0xFF);
			}
		}

		// 返回数值
		return n;
	}

	/**
	 * 转换字节数组为整数
	 * 
	 * @param bytes
	 *            需要转换的字节数组格式
	 * @param bigEndian
	 *            大端模式为true，小端模式为false
	 * @return 整形数值，返回-1表示转换失败
	 */
	public static long bytesToLong(byte[] bytes, boolean bigEndian) {
		if (bytes == null) {
			LogUtil.e(clazzName, "字节数组转成十进制整数时，数组为null");
			return -1;
		}
		if (bytes.length < 1) {
			LogUtil.e(clazzName, "字节数组转成十进制整数时，数组长度为0");
			return -1;
		}
		// 若只有一个字节
		if (bytes.length == 1) {
			return (int) (bytes[0] & 0xFF);
		}

		long n = 0;
		if (bigEndian) {
			// 大端模式
			n = bytes[0];
			for (int i = 1; i < bytes.length; i++) {
				n <<= 8;
				n |= bytes[i] & (0xFF);
			}
		} else {
			// 小端模式
			n = bytes[bytes.length - 1];
			for (int i = bytes.length - 2; i >= 0; i--) {
				n <<= 8;
				n |= bytes[i] & (0xFF);
			}
		}

		// 返回数值
		return n;
	}

	/**
	 * 转换字节数组为整数
	 * 
	 * @param bytes
	 *            需要转换的字节数组格式
	 * @return 整形数值，返回-1表示转换失败
	 */
	public static long bytesToLong(byte[] bytes) {
		return bytesToLong(bytes, true);
	}

	/**
	 * 转换Int整数为字节数组
	 * 
	 * @param number
	 *            需要转换的Int整数
	 * @param length
	 *            转换后字节数组的长度
	 * @param bigEndian
	 *            大端模式为true，小端模式为false
	 * @return 转化后的字节数组
	 */
	public static byte[] intToBytes(int number, int length, boolean bigEndian) {

		if (length < 1) {
			LogUtil.e(clazzName, "十进制int整数转成字节数组时，指定的数组长度非正");
			return null;
		}

		int temp = number;
		byte[] b = new byte[length];

		if (bigEndian) { // 大端模式
			for (int i = b.length - 1; i > -1; i--) {// 0 - (length-1)
				b[i] = (Integer.valueOf(temp & 0xff)).byteValue(); // 低位数据保留在数组的高位
				temp = temp >> 8; // 向右移8位
			}
		} else { // 小端模式
			for (int i = 0; i < b.length; i++) {
				b[i] = (Integer.valueOf(temp & 0xff)).byteValue(); // 低位数据保留在数组的低位
				temp = temp >> 8; // 向右移8位
			}
		}
		return b;
	}

	/**
	 * 转换Long整数为字节数组
	 * 
	 * @param number
	 *            需要转换的Long整数
	 * @param length
	 *            转换后字节数组的长度
	 * @param bigEndian
	 *            大端模式为true，小端模式为false
	 * @return 转化后的字节数组
	 */
	public static byte[] longToBytes(long number, int length, boolean bigEndian) {
		if (length < 1) {
			LogUtil.e(clazzName, "十进制long整数转成字节数组时，指定的数组长度非正");
			return null;
		}

		long temp = number;
		byte[] b = new byte[length];

		if (bigEndian) { // 大端模式
			for (int i = b.length - 1; i > -1; i--) {// 0 - (length-1)
				b[i] = (Long.valueOf(temp & 0xff)).byteValue(); // 低位数据保留在数组的高位
				temp = temp >> 8; // 向右移8位
			}
		} else { // 小端模式
			for (int i = 0; i < b.length; i++) {
				b[i] = (Long.valueOf(temp & 0xff)).byteValue(); // 低位数据保留在数组的低位
				temp = temp >> 8; // 向右移8位
			}
		}
		return b;
	}

	/**
	 * 转换Long整数为字节数组，默认为大端模式
	 * 
	 * @param number
	 *            需要转换的Long整数
	 * @param length
	 *            转换后字节数组的长度
	 * @return 转化后的字节数组
	 */
	public static byte[] longToBytes(long number, int length) {
		return longToBytes(number, length, true);
	}

	/**
	 * 转换Int整数为字节数组，默认为大端模式
	 * 
	 * @param number
	 *            需要转换的Int整数
	 * @param length
	 *            转换后字节数组的长度
	 * @return 转化后的字节数组
	 */
	public static byte[] intToBytes(int number, int length) {
		return intToBytes(number, length, true);
	}

	/**
	 * 十进制数转为ASCII码数组 例：567,则变为数组：0x35, 0x36, 0x37
	 * 
	 * @param dec
	 *            十进制数
	 * @return ASCII码数组
	 */
	public static byte[] longToAsciiBytes(long dec) {
		String s = dec + "";
		char[] c = s.toCharArray();
		int len = c.length;
		byte[] ascii = new byte[len];
		for (int i = 0; i < len; i++) {
			ascii[i] = (byte) (Integer.parseInt(c[i] + "") + 0x30);
		}

		return ascii;
	}

	/**
	 * 处理TLV格式的数组 1、判断data中的TAG是否是tag指定的 2、判断data长度是否正确
	 * 3、将data中value部分赋值到value字节数组中
	 * 
	 * @param data
	 *            TLV格式数组
	 * @param tag
	 *            标签TAG
	 * @param len
	 *            长度
	 * @param value
	 *            长度为len的字节数组，用于存放data中的值部分
	 * @return 处理结果
	 */
	public static boolean tlv(byte[] data, byte tag, byte len, byte[] value) {
		if (tag != data[0] || len != data[1] || data.length != 2 + len
				|| value.length != len) {
			return false;
		}

		for (int k = 0; k < len; k++) {
			value[k] = data[2 + k];
		}
		return true;
	}

	/**
	 * 逆转byte数组
	 * 
	 * @param arrays
	 *            byte数组 e.g. 0xE8,0x03,0x00,0x00 -> 0x00,0x00,0x03,0xE8
	 */
	public static void reverse(byte[] arrays) {
		int len = arrays.length;
		byte[] temp = FM_Bytes.copyOf(arrays, len);

		for (int i = 0; i < len; i++) {
			arrays[i] = temp[len - i - 1];
		}
	}

	/**
	 * 从源字节数组的开始截取指定长度的数组
	 * 
	 * @param original
	 *            源字节数据
	 * @param newLength
	 *            指定的长度
	 * @return 指定长度的字节数组
	 */
	public static byte[] copyOf(byte[] original, int newLength) {
		if (original == null) {
			throw new NullPointerException(" original Arrays is null");
		}

		byte[] result = new byte[newLength];

		if (original.length < newLength) {
			for (int i = 0; i < original.length; i++) {
				result[i] = original[i];
			}
		} else {
			for (int i = 0; i < newLength; i++) {
				result[i] = original[i];
			}
		}
		return result;

	}

	/**
	 * 从源字节数组的开始截取指定范围的字节数组
	 * 
	 * @param original
	 *            源字节数据
	 * @param from
	 *            范围的起始值
	 * @param to
	 *            范围的截止值
	 * @return 字节数组
	 */
	public static byte[] copyOfRange(byte[] original, int from, int to) {
		if (original == null) {
			throw new NullPointerException(" original Arrays is null");
		}

		if (to - from <= 0) {
			throw new IllegalArgumentException(" from[" + from + "]>to[" + to
					+ "]");
		}
		if (original.length < to || original.length < from) {
			throw new IllegalArgumentException(" ");
		}

		int newLength = to - from;
		byte[] result = new byte[newLength];

		for (int i = 0; i < newLength; i++) {
			result[i] = original[from + i];
		}

		return result;

	}

	/**
	 * 判断字节数组是不是以指定的字节数据结尾
	 * 
	 * @param data
	 *            字节数组
	 * @param end
	 *            指定的字节数据
	 * @return 判断结果
	 */
	public static boolean isEnd(byte[] data, byte[] end) {
		if (data == null || end == null) {
			return false;
		}

		if (data.length < end.length) {
			return false;
		}

		int j = data.length;
		for (int i = end.length - 1; i > 0; i--) {
			if (end[i] != data[--j]) {
				return false;
			}
		}
		return true;
	}

	/**
	 * 判断字节数组是不是以指定的字节数据结尾
	 * 
	 * @param data
	 *            字节数组
	 * @return 判断结果
	 */
	public static boolean isEnd9000(byte[] data) {
		byte[] end = new byte[] { (byte) 0x90, 0x00 };
		if (data == null || end == null) {
			return false;
		}

		if (data.length < end.length) {
			return false;
		}

		int j = data.length;
		for (int i = end.length - 1; i > 0; i--) {
			if (end[i] != data[--j]) {
				return false;
			}
		}
		return true;
	}

	/**
	 * byte数据补位
	 * 
	 * @param source
	 *            原始byte
	 * @param length
	 *            补位后的长度
	 * @param patchByte
	 *            补位的字节
	 */
	public static byte[] patch(byte[] source, int length, byte patchByte) {

		if (source == null || length <= 0) {
			return null;
		}
		int sourceLength = source.length;
		if (sourceLength >= length) {
			return source.clone();
		}

		byte[] result = new byte[length];
		for (int i = 0; i < sourceLength; i++) {
			result[i] = source[i];
		}
		for (int i = 0; i < (length - sourceLength); i++) {
			result[sourceLength + i] = patchByte;
		}
		return result;
	}

	/**
	 * 获取byte数组的奇偶校验
	 * 
	 * @param source
	 *            原始数据
	 * @return byte数组的奇偶校验
	 */
	public static byte getByteParity(byte[] source) {
		if (source == null || source.length == 0) {
			return (byte) 0xFF;
		}
		if (source.length == 1) {
			return source[0];
		}
		byte seed = source[0];
		for (int i = 1; i < source.length; i++) {
			seed ^= source[i];
		}
		return seed;
	}

	public static void main(String[] args) {
		String str = "3032";
		byte[] source = new byte[] { 0x01, 0x01, 0x00 };
		byte[] end = new byte[] { 0x04, (byte) 0x90, 0x00 };
		// System.out.println(source);

		byte result = FM_Bytes.getByteParity(source);
		System.out.println(FM_Bytes.bytesToHexString(new byte[] { result }));

	}

}