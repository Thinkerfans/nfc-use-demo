package com.fmsh.util;

import android.content.Context;
import android.nfc.Tag;

import com.android.common.utils.DialogUtils;
import com.android.common.utils.LogUtil;
import com.fmsh.nfcEeprom.NvmDMTech;


public class DataUtil {
	public static final String PREFS_NAME = "nnvm_demo1";
	public static final String TAG = "DataUtil";
	
	public static byte[] end = { (byte) 0xEF,(byte) 0x88 };
	public static byte Wifi_End = (byte)0xFE;

	public static byte[] transformData(byte[] data, int pageSize) {
		
		data = FM_Bytes.join(data, new byte[]{Wifi_End});
		int pageCount = data.length / pageSize;
		int lastPageDataLength = data.length % pageSize;
		if (lastPageDataLength != 0) {
			pageCount++;
		}

		byte[] result = new byte[pageCount * pageSize];
		for (int i = 0; i < data.length; i++) {
			result[i] = data[i];
		}
		for (int i = 0; i < (pageSize - lastPageDataLength); i++) {
			result[data.length + i] = 0;
		}

		return result;
	}
	
	public static final int PAGE_SIZE = 64;
	public static void writeWifiToDataMemory(Context ctx,Tag tag,byte[] data) {
		LogUtil.e(TAG, "enter method writeWifiToDataMemory ");

		NvmDMTech fm24 = null;
		try {
			data = DataUtil.transformData(data, PAGE_SIZE);
			data[63] = (byte)0xAA;
			
			fm24 = NvmDMTech.get(tag);
			LogUtil.e(TAG, "writeWifiToDataMemory method get fm24 "+fm24.toString());
			
			fm24.connect();
			
			if (data.length > 0) {
				int pageCount = data.length / PAGE_SIZE;
				for (int i = 0; i < pageCount; i++) {
					fm24.writePage(i, FM_Bytes.copyOfRange(
							data, i * PAGE_SIZE, i*PAGE_SIZE + PAGE_SIZE));
				}
			}					
			DialogUtils.showLongToast(ctx, "FM24NC128Tx WiFi设置成功");
		} catch (Exception e) {
			LogUtil.e(TAG, "FM24NC128Tx WIFi设置异常"+e.toString());
			DialogUtils.showLongToast(ctx, "FM24NC128Tx WIFi设置异常");
		} finally {
			try {
				if (fm24 != null) {
					fm24.close();
				}						
			} catch (Exception e) {
				LogUtil.e(TAG, "FM24NC128Tx  WIFi设置异常"+ e.toString());
			}
		}
	}

}
