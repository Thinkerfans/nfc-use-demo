package com.android.nfc.activity;

import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.common.utils.DialogUtils;
import com.android.common.utils.LogUtil;
import com.android.common.utils.WifiConnectManager;
import com.android.common.utils.WifiModel;
import com.fmsh.nfcEeprom.NvmDMTech;
import com.fmsh.nfcEeprom.R;
import com.fmsh.util.DataUtil;
import com.fmsh.util.FM_Bytes;
import com.fmsh.util.WifiUtils;
import com.fmsh.util.WifiUtils.ENCRYPTION_TYPE;

public class NFCCamSearch extends Activity {

	final static String TAG = "NFCCamSearch ";

	private TextView mTipTv;
	private NfcAdapter mNfcAdapter;
	private PendingIntent mNfcIntent;
	private IntentFilter[] mFilters;
	private String[][] mTechLists;
	protected Tag mNfcTag;
	protected Builder mDialogBuilder = null;
	protected Dialog mDialog;
	protected View mEditPwdView;
	private WifiConnectManager mWifiManager;
	private WifiModel mCurWifi;

	Button mBtRead, mBtWrite;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_homepage);
		LogUtil.e(TAG, " onCreate : ");
		mWifiManager = WifiConnectManager.getConnectManger(this);
		mCurWifi = mWifiManager.getWifiList().get(0);

		initView();
		initNfc();

	}

	@Override
	protected void onResume() {
		super.onResume();
		if (mNfcAdapter != null) {
			mNfcAdapter.enableForegroundDispatch(this, this.mNfcIntent,
					this.mFilters, this.mTechLists);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		if (mNfcAdapter != null) {
			mNfcAdapter.disableForegroundDispatch(this);
			mNfcTag = null;
		}
	}

	@Override
	protected void onNewIntent(Intent intent) {
		super.onNewIntent(intent);
		setIntent(intent);
		mNfcTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
		if (mNfcTag != null) {
			DialogUtils.showShortToast(this,
					R.string.nfc_find_tag + mNfcTag.toString());
			LogUtil.e(TAG, " tag : " + mNfcTag.toString());

			byte[] id = mNfcTag.getId();
			LogUtil.e(TAG, "tag id : " + FM_Bytes.bytesToHexString(id));
			mBtRead.setVisibility(View.VISIBLE);
			mBtWrite.setVisibility(View.VISIBLE);

		} else {
			DialogUtils.showShortToast(this, R.string.nfc_find_unknow_tag);
			LogUtil.e(TAG, " tag : is null");
		}

	}

	private void initNfc() {
		mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
		mNfcIntent = PendingIntent.getActivity(this, 0, new Intent(this,
				getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
		IntentFilter tech = new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED);
		this.mFilters = new IntentFilter[] { tech };
		this.mTechLists = new String[][] {
				new String[] { Ndef.class.getName() },
				new String[] { NdefFormatable.class.getName() } };

		if (mNfcAdapter == null) {
			DialogUtils.showShortToast(this, R.string.nfc_not_support);
			mTipTv.setText(R.string.nfc_not_support);
			return;
		}

		if (!mNfcAdapter.isEnabled()) {
			DialogUtils.showShortToast(this, R.string.nfc_not_open);
			mTipTv.setText(R.string.nfc_not_support);
			return;
		}
		mTipTv.setText(R.string.nfc_search);
	}

	private EditText mSSidEt, mPwdEt;

	private void initView() {

		mBtRead = (Button) findViewById(R.id.bt_read);
		mBtWrite = (Button) findViewById(R.id.bt_write);

		mBtRead.setVisibility(View.GONE);
		mBtWrite.setVisibility(View.GONE);

		mTipTv = (TextView) findViewById(R.id.tv_tip);
		mDialogBuilder = new AlertDialog.Builder(this);
		mEditPwdView = getLayoutInflater().inflate(R.layout.dialog_edit_layout,
				null);
		mSSidEt = (EditText) mEditPwdView.findViewById(R.id.et_ssid);
		if (mCurWifi != null) {
			mSSidEt.setText(mCurWifi.getSsid());
		}

		mPwdEt = (EditText) mEditPwdView.findViewById(R.id.et_pwd);
		mEditPwdView.findViewById(R.id.dialog_btn).setOnClickListener(
				new OnClickListener() {
					public void onClick(View v) {

						String ssid = mSSidEt.getText().toString();
						if (TextUtils.isEmpty(ssid)) {
							DialogUtils.showShortToast(NFCCamSearch.this,
									R.string.input_ssid);
							return;
						}
						String pwd = mPwdEt.getText().toString();
						if (TextUtils.isEmpty(pwd)) {
							DialogUtils.showShortToast(NFCCamSearch.this,
									R.string.input_pwd);
							return;
						}

						int defaultType = WifiModel.ENCRYPT_WPA;
						if (mCurWifi != null) {
							defaultType = mCurWifi.getType();
						}

						WifiUtils.ENCRYPTION_TYPE type = getEncryptType(defaultType);

						byte[] ndef = WifiUtils.wifiToNdef(ssid, pwd, type);
						DataUtil.writeWifiToDataMemory(NFCCamSearch.this,
								mNfcTag, ndef);
						mDialog.cancel();
					}
				});
	}

	private ENCRYPTION_TYPE getEncryptType(int type) {
		ENCRYPTION_TYPE re = null;
		if (type == WifiModel.ENCRYPT_WPA) {
			re = ENCRYPTION_TYPE.ENCRYPTION_TYPE_WPA;
		} else if (type == WifiModel.ENCRYPT_WEP) {
			re = ENCRYPTION_TYPE.ENCRYPTION_TYPE_WEP;
		} else {
			re = ENCRYPTION_TYPE.ENCRYPTION_TYPE_NONE;
		}

		return re;
	}

	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.bt_read:
			readTag();
			break;
		case R.id.bt_write:
			showWriteDialog();
			break;
		default:
			break;
		}
	}

	private void readTag() {
		NvmDMTech fm24 = NvmDMTech.get(mNfcTag);
		try {
			fm24.connect();
			byte[] data = fm24.readPage(0);
			if (data != null) {
				String s = new String(data);
				DialogUtils.showLongToast(this, "读出page one data : " + s);
				LogUtil.e(TAG, "page one data :" + s);
			}

		} catch (IOException e) {
			e.printStackTrace();
			DialogUtils.showLongToast(this, "读出page one data exception" );
			LogUtil.e(TAG, "page one exception :" + e.toString());
		} finally {
			try {
				if (fm24 != null) {
					fm24.close();
				}
			} catch (Exception e) {
				LogUtil.e(TAG, "FM24NC128Tx  WIFi设置异常" + e.toString());
			}
		}

	}

	private void showWriteDialog() {
		if (mDialog.isShowing()) {
			return;
		}
		mDialog = mDialogBuilder.setView(mEditPwdView).create();
		mDialog.show();
	}

}
