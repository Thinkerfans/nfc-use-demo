package com.android.common.utils;
import android.content.Context;
import android.widget.Toast;

public class DialogUtils {
	
	public static void showShortToast(Context context, String tip) {
		Toast.makeText(context, tip, Toast.LENGTH_SHORT).show();
	}

	public static void showLongToast(Context context, String tip) {
		Toast.makeText(context, tip, Toast.LENGTH_LONG).show();
	}

	public static void showShortToast(Context context, int tipId) {
		Toast.makeText(context, tipId, Toast.LENGTH_SHORT).show();
	}

	public static void showLongToast(Context context, int tipId) {
		Toast.makeText(context, tipId, Toast.LENGTH_LONG).show();
	}
	
}
