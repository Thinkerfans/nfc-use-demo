package com.android.common.utils;

import java.io.Serializable;

public class WifiModel implements Serializable {
	
	private static final long serialVersionUID = 6465439949642187929L;
	
	public static final int ENCRYPT_INVALID = -1;
	public static final int ENCRYPT_WEP = 0;
	public static final int ENCRYPT_WPA = 1;
	public static final int ENCRYPT_NOPASS = 2;
	
	private String bssid;
	private String ssid; // ssid 
	private int type;	
	private int auth;	
	private int encrypt; //  加密方式
	private int rssi;   // 信号强度
	
	private String pwd;
	
	
	public WifiModel(String ssid, int type, int rssi,int auth,int encrypt) {
		this.ssid = ssid;
		this.type = type;
		this.auth = auth;
		this.encrypt = encrypt;
		this.rssi = rssi;
	}
	
	public String getBssid() {
		return bssid;
	}
	public void setBssid(String bssid) {
		this.bssid = bssid;
	}
	public String getSsid() {
		return ssid;
	}
	public void setSsid(String ssid) {
		this.ssid = ssid;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public int getAuth() {
		return auth;
	}
	public void setAuth(int auth) {
		this.auth = auth;
	}
	public int getEncrypt() {
		return encrypt;
	}
	public void setEncrypt(int encrypt) {
		this.encrypt = encrypt;
	}
	
	public int getRssi() {
		return rssi;
	}
	public void setRssi(int rssi) {
		this.rssi = rssi;
	}
	
	@Override
	public boolean equals(Object o) {	
		if(o != null && o.getClass() == this.getClass()){
			WifiModel model = (WifiModel)o;
			if(model.getSsid() == null || ssid == null){
				return false;
			}else{
				return ssid.equalsIgnoreCase(model.getSsid());
			}
		}			
		return false;	
	}	
	
	@Override
	public int hashCode() {
		return ssid!=null?ssid.hashCode():0;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	@Override
	public String toString() {
		return "WifiModel [bssid=" + bssid + ", ssid=" + ssid + ", type="
				+ type + ", auth=" + auth + ", encrypt=" + encrypt + ", rssi="
				+ rssi + ", pwd=" + pwd + "]";
	}
	
	
	
}
