package com.android.common.utils;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

public class LogUtil {
	
	private static final boolean DEBUG = true ;
	private static final boolean WRITE_FILE_LOG = true;
	public static String SDCARD_DIR = Environment.getExternalStorageDirectory()
			.getAbsolutePath();

	
	public static void i(String tag , String msg){
		if (DEBUG) {
			Log.i(tag, msg);
			
			if (WRITE_FILE_LOG) {
				writeToLog("I/" + tag + ": " + msg);
			}
		}
	}
	public static void e(String tag , String msg){
		if (DEBUG) {
			Log.e(tag, msg);
			
			if (WRITE_FILE_LOG) {
				writeToLog("E/" + tag + ": " + msg);
			}
		}
	}
	public static void d(String tag , String msg){
		if (DEBUG) {
			Log.d(tag, msg);
			
			if (WRITE_FILE_LOG) {
				writeToLog("D/" + tag + ": " + msg);
			}
		}
	}
	public static void w(String tag , String msg){
		if (DEBUG) {
			Log.w(tag, msg);
			
			if (WRITE_FILE_LOG) {
				writeToLog("W/" + tag + ": " + msg);
			}
		}
	}
	
	public static void v(String tag , String msg){
		if (DEBUG) {
			Log.v(tag, msg);
			
			if (WRITE_FILE_LOG) {
				writeToLog("W/" + tag + ": " + msg);
			}
		}
	}
	
	private static Handler mLogHdlr = new FileLogHandler();
	
	private static void writeToLog(String log) {
		Message msg = mLogHdlr.obtainMessage();
		msg.obj = log;
		mLogHdlr.sendMessage(msg);
	}
	
	static class FileLogHandler extends Handler {
		private boolean				mHasSDCard	= true;
		private FileOutputStream	mLogOutput;
		private File				mLogFile;
		
		FileLogHandler() {
			mHasSDCard = hasExternalStorage();
			
			if (mHasSDCard) {
				try {
					mLogFile = new File(SDCARD_DIR, "nfclog.txt");				
					if (!mLogFile.exists()) {
						mLogFile.createNewFile();
					}
				} catch (IOException e) {}
			}
		}
		
		boolean hasExternalStorage(){
			return Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState());
		}
		
		public void handleMessage(Message msg) {
			if (!mHasSDCard) {
				return;
			}

			try {
				String log = (String) msg.obj + "\n";
				if (log != null) {
					byte[] logData = log.getBytes();
					getLogOutput().write(logData, 0, logData.length);
				}
			} catch (Exception e) {
			}
		}

		FileOutputStream getLogOutput() throws Exception {
			if (mLogOutput == null) {
				mLogOutput = new FileOutputStream(mLogFile, true);
			}
			
			return mLogOutput;
		}
	}
}
