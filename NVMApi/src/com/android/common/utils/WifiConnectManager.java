package com.android.common.utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;

public class WifiConnectManager {



	private final static String TAG = "WifiConnectManager";
	
	private final static String APCAM_ = "IPCAM_";

	private final static int INVALID_ID = -1;

	private WifiManager mWifiManager;

	private ScanResultComparator mComparator;

	private List<WifiModel> mWifiList = new ArrayList<WifiModel>();

	private static WifiConnectManager mWifConnectManager ;
	
	private WifiConnectManager(Context ctx) {
		init(ctx);
	}

	public static  WifiConnectManager getConnectManger(Context ctx) {
	
		if(mWifConnectManager == null){
			mWifConnectManager = new WifiConnectManager(ctx);
		}
		return mWifConnectManager;
	}

	private void init(Context ctx) {
		if (mWifiManager == null) {
			this.mWifiManager = (WifiManager) ctx
					.getSystemService(Context.WIFI_SERVICE);
		}
		if (mComparator == null) {
			mComparator = new ScanResultComparator();
		}
	}

	public boolean openWifi() {
		if (!mWifiManager.isWifiEnabled()) {
			return mWifiManager.setWifiEnabled(true);
		}
		return true;
	}

	public boolean closeWifi() {
		if (mWifiManager.isWifiEnabled()) {
			return mWifiManager.setWifiEnabled(false);
		}
		return true;
	}

	public boolean reScan() {
		return mWifiManager.startScan();
	}
	
	public int getCurWifiId(){
		int id = -1;
		WifiInfo info = mWifiManager.getConnectionInfo();
		if(info != null){
			id = info.getNetworkId();
		}
		return id;
	}
	
	public void removeWifiById(int id){
		LogUtil.e("wifi id", ""+id);
		mWifiManager.removeNetwork(id);

	}

	public List<WifiModel> getWifiList() {
		refreshWifiList();
		return mWifiList;
	}

	private void refreshWifiList() {
		mWifiList.clear();
		reScan();
		List<ScanResult> scanList = mWifiManager.getScanResults();
		WifiInfo info = mWifiManager.getConnectionInfo();
		String curWifiSsid = "";
		WifiModel curWifi = null;
		if (info != null) {
			curWifiSsid = info.getSSID();
		}

		for (ScanResult sr : scanList) {
				
			WifiModel model =  createWifiModel(sr);
			
			if (mWifiList.contains(model)) {
				mWifiList.remove(model);
			}

			if (sr.SSID.equals(curWifiSsid)) {
				curWifi = model;
			} else {
				mWifiList.add(model);
			}
		}

		if (!mWifiList.isEmpty()) {
			Collections.sort(mWifiList, mComparator);
		}

		if (curWifi != null) {
			mWifiList.add(0, curWifi);
		}
	}

	private WifiModel createWifiModel(ScanResult sr){
		int rssi = WifiManager.calculateSignalLevel(sr.level, 100);
		int type = getCipherType(sr);
		int auth = 0;
		int	encrypt = 0;
		if(type == WifiModel.ENCRYPT_WPA){
		     auth = getWifiAuth(sr);
			if(auth==4)
				encrypt = 3;
			else if(auth==3)
				encrypt =2;
		}else if(type == WifiModel.ENCRYPT_WEP){
			encrypt = 1;
			auth = 2;
		}
				
		return new WifiModel(sr.SSID, type, rssi,auth,encrypt);
	}
	
	public List<WifiModel> getApCamList() {
		refreshWifiList();
		List<WifiModel> apCamList = new ArrayList<WifiModel>();
		for(WifiModel  m: mWifiList){		
			if(m.getSsid().contains(APCAM_)){
				apCamList.add(m);
			}
		}
		return apCamList;
	}

	// 提供一个外部接口，传入要连接的无线网
	public boolean connectWifi(String ssid, String pwd, int type) {

		WifiConfiguration wifiConfig = this.createWifiConfig(ssid, pwd, type);
		if (wifiConfig == null) {
			return false;
		}

		WifiConfiguration tempConfig = this.isExsits(ssid);
		if (tempConfig != null) {
			mWifiManager.removeNetwork(tempConfig.networkId);
		}

		int id = mWifiManager.addNetwork(wifiConfig);
		LogUtil.e(TAG, "connectWifi = id" + id);
//		if (id != INVALID_ID) {
			return mWifiManager.enableNetwork(id, true);
//		}

//		return false;
	}

	private int getCipherType(ScanResult sr) {
		int encrypt = WifiModel.ENCRYPT_NOPASS;
		if (sr.capabilities.contains("WPA")) {
			encrypt = WifiModel.ENCRYPT_WPA;
		} else if (sr.capabilities.contains("WEP")) {
			encrypt = WifiModel.ENCRYPT_WEP;
		}
		return encrypt;
	}
	
	public static int getWifiAuth(ScanResult re){
		if(re.capabilities.contains("WPA2-PSK"))
			return 4;
		else if(re.capabilities.contains("WPA-PSK"))
			return 3;
		else
			return 0;
	}

	// 查看以前是否也配置过这个网络
	private WifiConfiguration isExsits(String ssid) {
		List<WifiConfiguration> configs = mWifiManager.getConfiguredNetworks();
		for (WifiConfiguration config : configs) {
			if (config.SSID.equals("\"" + ssid + "\"")) {
				return config;
			}
		}
		return null;
	}

	private WifiConfiguration createWifiConfig(String ssid, String pwd, int type) {
		LogUtil.e(TAG, "SSID = " + ssid + " Password =" + pwd + " wifi type ="
				+ type);
		WifiConfiguration config = new WifiConfiguration();
		config.SSID = "\"" + ssid + "\"";

		if (type == WifiModel.ENCRYPT_NOPASS) {
			// config.wepKeys[0] = "";
			// config.wepTxKeyIndex = 0;
			config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
		} else if (type == WifiModel.ENCRYPT_WEP) {
			config.preSharedKey = "\"" + pwd + "\"";
			config.hiddenSSID = true;
			config.allowedAuthAlgorithms
					.set(WifiConfiguration.AuthAlgorithm.SHARED);
			config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.CCMP);
			config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
			config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.WEP40);
			config.allowedGroupCiphers
					.set(WifiConfiguration.GroupCipher.WEP104);
			config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.NONE);
			config.wepTxKeyIndex = 0;
		} else if (type == WifiModel.ENCRYPT_WPA) {
			config.preSharedKey = "\"" + pwd + "\"";
			config.hiddenSSID = true;
			// config.allowedAuthAlgorithms
			// .set(WifiConfiguration.AuthAlgorithm.OPEN);
			// config.allowedGroupCiphers.set(WifiConfiguration.GroupCipher.TKIP);
			// config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
			// config.allowedPairwiseCiphers
			// .set(WifiConfiguration.PairwiseCipher.TKIP);
			// config.allowedProtocols.set(WifiConfiguration.Protocol.WPA);
			// config.status = WifiConfiguration.Status.ENABLED;
		} else {
			return null;
		}
		return config;
	}

	private static class ScanResultComparator implements Comparator<WifiModel> {
		public int compare(WifiModel lhs, WifiModel rhs) {
			if (lhs.getRssi() > rhs.getRssi()) {
				return -1;
			} else if (lhs.getRssi() == rhs.getRssi()) {
				return 0;
			} else {
				return 1;
			}
		}

	}
}
